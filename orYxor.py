
patEspA=[0,1,10,11]  # OR CAMBIA 11 -> 1 Y XOR 11 -> 0
patEspB=[0,1,1,1]

w0=1.5
w1=0.5   # ENTRADAS PRE DEFINIDAS
w2=1.5


def setXS(pos):
    print("-----------------------------------------------------")
    if(pos==0):
        
        print("\nReinicia: \n     Patrón -----------> 00")
        print("     Entradas x1 = 0, x2 = 0, x0 = 1")
    elif(pos==1):
        print("     Patrón -----------> 01")
        print("     Entradas x1 = 0, x2 = 1, x0 = 1")
    elif(pos==2):
        print("     Patrón -----------> 10")
        print("     Entradas x1 = 1, x2 = 0, x0 = 1")
    elif(pos==3):
        print("     Patrón -----------> 11")
        print("     Entradas x1 = 1, x2 = 1, x0 = 1")
    if(pos==4):
        print("****** FIN ******")
        
    else:
        x0=1
        x1=0
        x2=0        
        if(patEspA[pos]==0):
            x1=0
            x2=0
                
        elif(patEspA[pos]==1):
            x1=0
            x2=1
        elif(patEspA[pos]==10):
            x1=1
            x2=0
        else:
            x1=1
            x2=1
        Operacion(x0,x1,x2,pos)



def Operacion(x0,x1,x2,pos):
    global w1
    global w2
    global w0
    salida = patEspB[pos]
    

    net = x1*w1 + x2*w2 + x0*w0
    prod = 0
    if (net >= 0):
        prod=1
    print("     --------> Net: "+str(net))
    print("     --------> Produce: "+str(prod))
    error = salida-prod
    print("     --------> Error: "+str(error))

    if(error!=0):
        print("     ----------------->Reajuste/hay error")
        lista = AjustePesos(error,x0,x1,x2)
        w0=lista[2]
        w1=lista[0]
        w2=lista[1]
        print("     ----------------->Se actualiza w0 -> "+ str(lista[2]))
        print("     ----------------->Se actualiza w1 -> "+ str(lista[0]))
        print("     ----------------->Se actualiza w2 -> "+ str(lista[1]))        
        setXS(0)
        print("-----------------------------------------------------")
    else:
        pos+=1
        print("     ********Sin error en patrón")
        setXS(pos)       


    
        
            


def AjustePesos(error,x0,x1,x2):
    global w1
    global w2
    global w0
    
    w1 = w1+ (error)*x1
    w2 = w2+ (error)*x2
    w0 = w0+ (error)*x0

    lista = [w1,w2,w0]
    
    return lista



setXS(0)
    
